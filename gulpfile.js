require('dotenv').config();
var gulp = require('gulp');
var concat = require('gulp-concat');
var templateCache = require('gulp-angular-templatecache');
var annotate = require('gulp-ng-annotate');
var del = require('del');
var mergeStream = require('merge-stream');
var order = require('gulp-order');
var sass = require('gulp-sass');
var connect = require('gulp-connect');
var filesort = require('gulp-angular-filesort');
var cors = require('cors');
var sourcemaps = require('gulp-sourcemaps');
// var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var rename = require('gulp-rename');
var minimist = require('minimist');
var gulpif = require('gulp-if');
var preprocess = require('gulp-preprocess');



CONFIG = {
  destination: 'dist',
  htmlmin: {
    removeComments: true,
    collapseWhitespace: true,
    collapseBooleanAttributes: true,
    removeAttributeQuotes: true
  },
  sass: {
    outputStyle: (process.env.NODE_ENV === 'production') ? 'compressed' : 'expanded',
  },
  uglify: {
    mangle: true
  },
  preprocessContext: {}
};

FILES = {
  js: 'src/**/*.js',
  sass: 'src/**/*.scss',
  templates: 'src/**/*.tpl.html',
  html: 'src/index.html',
  favicon: 'favicon/*',
  bundleJs: [
    'node_modules/angular/angular.min.js',
    'node_modules/angular-resource/angular-resource.min.js',
    'node_modules/angular-animate/angular-animate.min.js',
    'node_modules/angular-aria/angular-aria.min.js',
    'node_modules/angular-messages/angular-messages.min.js',
    'node_modules/angular-ui-router/release/angular-ui-router.min.js',
    'node_modules/angular-material/angular-material.min.js',
    'node_modules/angular-material-autocomplete-list/dist/autocompleteList.min.js'
  ],
  bundleCss: [
    'node_modules/angular-material/angular-material.min.css'
  ],
  allSrc: 'src/**/*',

  compiledCss: CONFIG.destination + '/**/*.css',
  unminifiedJs: CONFIG.destination + '/cheerManager.js',
  compiledJs: CONFIG.destination + '/**/*.js',
  compiledHtml: CONFIG.destination + '/index.html',
  copiedStatic: CONFIG.destination + '/favicon'
};



gulp.task('compile-bundle-css', ['clean-css'], function() {
  return gulp.src(FILES.bundleCss)
    .pipe(concat('bundle.css'))
    .pipe(gulp.dest(CONFIG.destination));
});


gulp.task('compile-sass', ['clean-css'], function() {
  return gulp.src(FILES.sass)
    .pipe(sourcemaps.init())
    .pipe(sass(CONFIG.sass).on('error', sass.logError))
    .pipe(concat('cheerManager.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(CONFIG.destination));
});


gulp.task('compile-css', ['clean-css', 'compile-bundle-css', 'compile-sass']);


gulp.task('clean-css', function() {
  return del(FILES.compiledCss);
});


gulp.task('compile-html', ['clean-html'], function() {
  return gulp.src(FILES.html)
    .pipe(preprocess({context: CONFIG.preprocessContext}))
    .pipe(gulpif(process.env.NODE_ENV === 'production', htmlmin(CONFIG.htmlmin)))
    .pipe(gulp.dest(CONFIG.destination))
});


gulp.task('clean-html', function() {
  return del(FILES.compiledHtml);
});


gulp.task('compile-angular', ['clean-js'], function() {
  if (process.env.NODE_ENV === 'production') {
    return gulp.src(FILES.js)
      .pipe(sourcemaps.init())
      .pipe(preprocess({context: CONFIG.preprocessContext}))
      .pipe(filesort())
      .pipe(annotate())
      .pipe(concat('cheerManager.js'))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(CONFIG.destination))
      // Minification
      // The entire pipe chain has to be duplicated due to a bug
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(uglify(CONFIG.uglify))
      .pipe(rename({extname: '.min.js'}))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(CONFIG.destination));
  }
  else {
    return gulp.src(FILES.js)
      .pipe(sourcemaps.init())
      .pipe(preprocess({context: CONFIG.preprocessContext}))
      .pipe(filesort())
      .pipe(annotate())
      .pipe(concat('cheerManager.js'))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(CONFIG.destination))
  }
});


gulp.task('compile-templates', ['compile-angular'], function(done) {
  function templates() {
    return gulp.src(FILES.templates)
      .pipe(preprocess({context: CONFIG.preprocessContext}))
      .pipe(gulpif(process.env.NODE_ENV === 'production', htmlmin(CONFIG.htmlmin)))
      .pipe(templateCache({
        module: 'cheerManager',
        transformUrl: function(url) {
          var index = url.lastIndexOf('/');
          return (index >= 0) ? url.slice(index + 1) : url;
        }
      }));
  }

  var numStreams = 0;
  function streamDone(input) {
    numStreams += 1;
    if (numStreams === 2) {
      done();
    }

    return input;
  }

  // Concatenate the templates onto the end of minified and unminified scripts
  var unminifiedStream = mergeStream(templates(), gulp.src(CONFIG.destination + '/cheerManager.js'))
    .pipe(order([
      '!template.js',
      'template.js'
    ]))
    .pipe(concat('cheerManager.js'))
    .pipe(gulp.dest(CONFIG.destination));

  if (process.env.NODE_ENV === 'production') {
    var minifiedStream = mergeStream(templates(), gulp.src(CONFIG.destination + '/cheerManager.min.js'))
      .pipe(gulp.dest('temp/'))
      .pipe(order([
        '!template.js',
        'template.js'
      ]))
      .pipe(concat('cheerManager.min.js'))
      .pipe(gulp.dest(CONFIG.destination));

    return mergeStream(unminifiedStream, minifiedStream);
  }
  else {
    return unminifiedStream;
  }
});





gulp.task('bundle-js', ['clean-js'], function() {
  return gulp.src(FILES.bundleJs)
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest(CONFIG.destination));
});


gulp.task('compile-js', ['compile-angular', 'compile-templates', 'bundle-js'], function() {
  gulp.src(FILES.compiledJs).pipe(connect.reload());
});
gulp.task('clean-js', function() {
  return del(FILES.compiledJs);
});


gulp.task('copy-favicon', ['clean-favicon'], function() {
  gulp.src(FILES.favicon)
    .pipe(gulp.dest(CONFIG.destination + '/favicon/'));
});
gulp.task('clean-favicon', function() {
  return del(FILES.copiedFavicon);
});


// gulp.task('jshint', function() {
//   return gulp.src(FILES.jsFiles)
//     .pipe(jshint())
//     .pipe(jshint.reporter('jshint-stylish'))
//     .pipe(jshint.reporter('fail'));
// });


gulp.task('build', ['compile-js', 'compile-css', 'compile-html', 'copy-favicon'], function() {
  gulp.src(FILES.allSrc)
    .pipe(connect.reload());
});


gulp.task('serve', ['watch'], function() {
  connect.server({
    root: 'dist',
    fallback: 'dist/index.html',
    livereload: true,
    port: process.env.GULP_CONNECT_PORT || 8080,
    middleware: function(req, res, next) {
      return [cors()];
    }
  });
});


gulp.task('watch', ['build'], function() {
  return gulp.watch(FILES.allSrc, ['build']);
});


gulp.task('default', ['watch', 'serve']);
