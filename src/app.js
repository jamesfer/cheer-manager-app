angular.module('cheerManager', [
	'ui.router',
	'ngMaterial',
	'cheerManager.core',
	'cheerManager.teams',
	'cheerManager.members',
	'cheerManager.competitions',
	'cheerManager.accounts'
	// 'cheerManager.home'
])

// Config
.config(function($locationProvider, $mdThemingProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/accounts');
	$locationProvider.html5Mode(true);

	$mdThemingProvider.theme('default')
		.primaryPalette('red', {
			'default': 'A700'
		})
		.accentPalette('grey');
})

.run(function($http) {
	$http.defaults.useXDomain = true;
});
