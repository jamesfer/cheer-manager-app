angular.module('cheerManager.members.common.states', [])

.config(function($stateProvider) {
  $stateProvider
    .state('membersIndex', {
      url: '/members',
      title: 'Members Index',
      templateUrl: 'members-index-view.tpl.html',
      controller: 'membersIndexCtrl',
      resolve: {
        membersList: function(Member) {
          "ngInject";
          return Member.query().$promise;
        }
      }
    })
    .state('membersAdd', {
      url: '/members/add',
      title: 'Create New Member',
      templateUrl: 'members-edit-view.tpl.html',
      controller: 'membersEditCtrl',
      resolve: {
        member: function() { return {}; }
      }
    })
    .state('membersDetail', {
      url: '/members/:id',
      title: 'Member Detail',
      templateUrl: 'members-detail-view.tpl.html',
      controller: 'membersDetailCtrl',
      resolve: {
        member: function($stateParams, Member) {
          "ngInject";
          return Member.get({id: $stateParams.id}).$promise;
        }
      }
    })
    .state('membersEdit', {
      url: '/members/:id/edit',
      title: 'Edit Member',
      templateUrl: 'members-edit-view.tpl.html',
      controller: 'membersEditCtrl',
      resolve: {
        member: function($stateParams, Member) {
          "ngInject";
          return Member.get({id: $stateParams.id}).$promise;
        }
      }
    });
});
