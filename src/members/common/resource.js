angular.module('cheerManager.members.common.resource', [
  'cheerManager.common.resourcePlus'
])

.factory('Member', function(resourcePlus) {
  return resourcePlus('/api/members/:id', {id: '@id'}, {
    bulkAdd: {
      method: 'POST',
      url: '/api/members/bulkAdd'
    }
    // getTeams: {
    //   method: 'GET',
    //   url: '/api/members/:id/teams',
    //   isArray: true
    // },
    // getPayments: {
    //   method: 'GET',
    //   url: '/api/members/:id/payments',
    //   isArray: true
    // },
    // getTrainings: {
    //   method: 'GET',
    //   url: '/api/members/:id/trainings',
    //   isArray: true
    // }
  });
});
