angular.module('cheerManager.members.views.detail', [])


.controller('membersDetailCtrl', function($scope, title, member) {
  $scope.member = member;
  title.pushValue($scope.member.firstName + ' ' +  $scope.member.lastName);
});
