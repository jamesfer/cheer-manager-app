angular.module('cheerManager.members.views.edit', [])

.controller('membersEditCtrl', function($scope, $stateParams, $state, Member, member) {
	$scope.isLoading = false;
  $scope.serverErrors = [];

	// The empty member object
	$scope.member = member;
	$scope.isNew = true;

	// Check if an id has been specified in the url
	if ($stateParams.id !== undefined) {
		$scope.isNew = false;
	}


	// Called when the form should be submitted
	$scope.submit = function() {
		// Start loading
		$scope.isLoading = true;
		var memberId = null;

		Member.save($scope.member).$promise
			.then(function(value) {
				memberId = value.id;
			})
			.then(function() {
				// Success
				$scope.serverErrors = [];

				// Redirect to detail
				$state.go('membersDetail', {id: memberId});
			})
			.catch(function(httpResponse) {
				// Failure
				console.log('request failed');
				if (httpResponse.data.errors) {
					$scope.serverErrors = httpResponse.data.errors;
				}
				$scope.isLoading = false;
			})
			.finally(function() {
				$scope.isLoading = false;
			});
	};

	// Called when the member should be deleted
	$scope.delete = function() {
		// This can only be called if the member already exists
		if (!$scope.isNew) {
			// Start loading
			$scope.isLoading = true;

			Member.delete({id: $scope.member.id}).$promise
				.then(function(value) {
					// Success
					$scope.serverErrors = [];

					// Redirect to index
					$state.go('membersIndex');
				})
	      .catch(function(httpResponse) {
					console.log('Deletion failed');
				})
	      .finally(function() {
	        $scope.isLoading = false;
	      });
		}
	};
});
