angular.module('cheerManager.members.views', [
  'cheerManager.members.views.index',
  'cheerManager.members.views.detail',
  'cheerManager.members.views.edit',
  'cheerManager.members.views.bulkAdd'
]);
