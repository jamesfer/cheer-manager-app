angular.module('cheerManager.members.views.bulkAdd', [
  'ui.router',
  'ngMessages'
])

.config(function($stateProvider) {
  $stateProvider
    .state('membersBulkAdd', {
      url: '/members/bulkAdd',
      title: 'Bulk create members',
      templateUrl: 'members-bulk-add-view.tpl.html',
      controller: 'membersBulkAddCtrl'
    });
})

.controller('membersBulkAddCtrl', function($scope, $state, Member) {
  $scope.csvString = "";

  $scope.submit = function() {
    $scope.loading = true;

    Member.bulkAdd({members: $scope.csvString}).$promise
      .then(function() {
        // Successful
        // Redirect to index
        $state.go('membersIndex');
      })
      .catch(function() {
        // Error
      })
      .finally(function() {
        $scope.loading - false
      });
  }
});
