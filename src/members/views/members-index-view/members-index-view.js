angular.module('cheerManager.members.views.index', [])

.controller('membersIndexCtrl', function($scope, Member, membersList) {
	$scope.members = membersList;
});
