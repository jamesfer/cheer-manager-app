angular.module('cheerManager.teams.common.resource', [
  'cheerManager.common.resourcePlus'
])

.factory('Team', function(resourcePlus) {
  return resourcePlus('/api/teams/:id', {id: '@id'}, {
    saveMembers: {
      method: 'PUT',
      url: '/api/teams/:id/members'
    },
    getMembers: {
      method: 'GET',
      url: '/api/teams/:id/members',
      isArray: true
    }
  });
});
