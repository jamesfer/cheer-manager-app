angular.module('cheerManager.teams.common.states', [
  'ui.router',
  'cheerManager.teams.common.resource'
])

.config(function($stateProvider) {
  $stateProvider
    .state('teamsIndex', {
      url: '/teams',
      title: 'Team Index',
      templateUrl: 'teams-index-view.tpl.html',
      controller: 'teamsIndexCtrl',
      resolve: {
        teams: function(Team) {
          "ngInject";
          return Team.query().$promise;
        }
      }
    })
    .state('teamsAdd', {
      url: '/teams/add',
      title: 'Create New Team',
      templateUrl: 'teams-edit-view.tpl.html',
      controller: 'teamsEditCtrl',
      resolve: {
        team: function() {
          // Return a blank team object
          return {members: []};
        },
        allMembers: function(Member) {
          "ngInject";
          return Member.query().$promise;
        }
      }
    })
    .state('teamsDetail', {
      url: '/teams/:id',
      title: 'Team Detail',
      templateUrl: 'teams-detail-view.tpl.html',
      controller: 'teamsDetailCtrl',
      resolve: {
        team: function($stateParams, $q, Team) {
          "ngInject";
          // Fetches both the team and member details.
          // Then assigns the members array onto the members property of the team and returns the team.
          // The final object should look like:
          // team: {
          //   ...
          //   members: [...]
          // }
          return $q.all({
            team: Team.get({id: $stateParams.id}).$promise,
            members: Team.getMembers({id: $stateParams.id}).$promise
          }).then(function(hash) {
            hash.team.members = hash.members;
            return hash.team;
          });
        }
      }
    })
    .state('teamsEdit', {
      url: '/teams/:id/edit',
      title: 'Edit Team',
      templateUrl: 'teams-edit-view.tpl.html',
      controller: 'teamsEditCtrl',
      resolve: {
        team: function($stateParams, $q, Team) {
          "ngInject";
          return $q.all({
            team: Team.get({id: $stateParams.id}).$promise,
            teamMembers: Team.getMembers({id: $stateParams.id}).$promise,
          }).then(function(hash) {
            hash.team.members = hash.teamMembers;
            return hash.team;
          });
        },
        allMembers: function(Member) {
          "ngInject";
          return Member.query().$promise
        }
      }
    });
});
