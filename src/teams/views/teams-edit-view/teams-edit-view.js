angular.module('cheerManager.teams.views.edit', [
	'autocompleteList'
])

.controller('teamsEditCtrl', function($scope, $stateParams, $state, Team, team, allMembers) {
	$scope.isLoading = false;
	$scope.serverErrors = [];

	// The empty team object
	$scope.team = team;
	$scope.allMembers = allMembers;
	$scope.isNew = true;

	// Check if an id has been specified in the url
	if ($stateParams.id !== undefined) {
		$scope.isNew = false;
	}


	// Called when the form should be submitted
	$scope.submit = function() {
		// Start loading
		$scope.isLoading = true;
		var teamId = null;

		Team.save($scope.team).$promise
			.then(function(value) {
				teamId = value.id;

				memberIds = [];
				$scope.team.members.forEach(function(member) {
					memberIds.push(member.id);
				});

				// Now save the members
				return Team.saveMembers({id: teamId}, {members: $scope.team.members}).$promise;
			})
			.then(function() {
				// Success
				$scope.serverErrors = [];

				// Redirect to detail
				$state.go('teamsDetail', {id: teamId});
			})
			.catch(function(httpResponse) {
				// Failure
				console.log('request failed');
				if (httpResponse.data.errors) {
					$scope.serverErrors = httpResponse.data.errors;
				}
				$scope.isLoading = false;
			})
			.finally(function() {
				$scope.isLoading = false;
			});
	};

	// Called when the team should be deleted
	$scope.delete = function() {
		// This can only be called if the team already exists
		if (!$scope.isNew) {
			// Start loading
			$scope.isLoading = true;

			Team.delete({id: $scope.team.id}).$promise
				.then(function(value) {
					// Success
					$scope.serverErrors = [];

					// Redirect to index
					$state.go('teamsIndex');
				})
	      .catch(function(httpResponse) {
					console.log('Deletion failed');
				})
	      .finally(function() {
	        $scope.isLoading = false;
	      });
		}
	};
});
