angular.module('cheerManager.teams.views', [
  'cheerManager.teams.views.detail',
  'cheerManager.teams.views.index',
  'cheerManager.teams.views.edit'
]);
