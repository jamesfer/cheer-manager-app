angular.module('cheerManager.teams.views.detail', [])

.controller('teamsDetailCtrl', function($scope, title, team) {
	// The value of the team fetched during the resolve stage of the route
	// The members property contains an array of all members on this team
	$scope.team = team;
	title.pushValue($scope.team.name);
});
