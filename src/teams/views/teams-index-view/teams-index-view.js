angular.module('cheerManager.teams.views.index', [])

.controller('teamsIndexCtrl', function($scope, $mdDialog, teams) {
	// The list of teams fetched during the resolve stage of the route
	$scope.teams = teams;
});
