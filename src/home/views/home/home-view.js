angular.module('cheerManager.home.views.home', [
  'ui.router'
])

.config(function($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      title: 'Swinburne Team Manager',
      templateUrl: 'home-view.tpl.html',
      controller: 'homeCtrl',
      resolve: {
        teamsList: function(Member) {
          // return Member.getTeams({id: user.id}).$promise;
          return [];
        },
        paymentsList: function(Member) {
          // return Member.getPayments({id: user.id}).$promise;
          return [];
        },
        trainingsList: function(Member) {
          // return Member.getTrainings({id: user.id}).$promise;
          return [];
        }
      }
    });
})

.controller('homeCtrl', function($scope, teamsList, paymentsList, trainingsList) {
  $scope.teamsList = teamsList;
  $scope.paymentsList = paymentsList;
  $scope.trainingsList = trainingsList;
});
