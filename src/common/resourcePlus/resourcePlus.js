angular.module('cheerManager.common.resourcePlus', [
  'ngResource',
  'cheerManager.common.apiUrl'
])

/**
 * Replacement for the default resource type that prepends
 * the api address onto the resource url.
 */
.factory('resourcePlus', function($resource, apiUrl) {
  return function(url, paramDefaults, actions) {
    // Prepend the api url
    url = apiUrl + url;

    for (key in actions) {
      if (actions.hasOwnProperty(key) && actions[key].url) {
        actions[key].url = apiUrl + actions[key].url;
      }
    };

    return $resource(url, paramDefaults, actions);
  };
});
