angular.module('cheerManager.core.sidenav', [])

.service('sidenav', function($mdSidenav, $location, $q, $rootScope) {
	// All menu links
	var menuItemsTemplate = [
		// {
		// 	'title': 'Home',
		// 	'path': '/',
		// 	'icon': '#'
		// },
		{
			'title': 'Members',
			'path': '/members',
			'icon': '#'
		},
		{
			'title': 'Teams',
			'path': '/teams',
			'icon': '#'
		},
		{
			'title': 'Competitions',
			'path': '/competitions',
			'icon': '#'
		},
		{
			'title': 'Accounts',
			'path': '/accounts',
			'icon': '#'
		}
	];
	var menuItems = [];


	function loadMenuItems() {
		// The list of links. Stored off the scope to save some digest cycles.
		var menuItems = [];

		// Will check the current item and then recursively call itself
		// with the next item until the end of the template list
		function addItem(index) {
			// Stop if we reached the end of the array.
			if (index >= menuItemsTemplate.length) {
				return $q.resolve();
			}

			// No permission needed
			if (menuItemsTemplate[index].permission === undefined) {
				menuItems.push(menuItemsTemplate[index]);
				// Add the next item
				return addItem(index + 1);
			}
			else {
				// Check for the permission
				// return User.hasPermission(menuItemsTemplate[index].permission)
					// .then(function(hasPermission) {
						// if (hasPermission) {
							menuItems.push(menuItemsTemplate[index]);
						// }

						// Add the next item
						return addItem(index + 1);
					// });
			}
		}

		// Recursively load all menu items
		return addItem(0)
			.then(function() {
				// Now that all links have been checked, add them to the scope
				return menuItems;
			});
	}


	function setMenuItems(items) {
		menuItems = items;
	}


	// Immediately attempt to load the menu items
	// User.isLoggedIn()
		// .then(function(isLoggedIn) {
			// if (isLoggedIn) {
				// Load the menu links straight away
				loadMenuItems().then(setMenuItems);
			// }
			// else {
				// Otherwise wait for the login event
				// $rootScope.$on('userLoggedIn', function() {
					// loadMenuItems().then(setMenuItems);
				// });
			// }
		// });


	this.getMenuItems = function() {
		return menuItems;
	};


	// Redirects the app to a url
	// Function is used in the sidebar to automatically close it when a link is clicked
	this.navigateTo = function(address) {
		$mdSidenav('left').close();
		$location.path(address);
	};
});
