angular.module('cheerManager.core.title', [])

.service('title', function($state, $rootScope) {
	var titleList = [];

	// Clear the list when the route changes
	$rootScope.$on('$stateChangeStart', function() {
		titleList = [];
	});

	this.value = function() {
		if (titleList.length > 0) {
			return titleList[titleList.length - 1];
		}
		else if ($state.current !== undefined && $state.current.title !== undefined) {
			return $state.current.title;
		}
		else {
			return '';
		}
	};

	this.pushValue = function(newValue) {
		titleList.push(newValue);
	};

	this.popValue = function() {
		titleList.pop();
	};
});
