angular.module('cheerManager.core.uiCtrl', [
	// 'ngMaterial'
])

.controller('uiCtrl', function($scope, $mdSidenav, $mdMedia, $location, $rootScope, sidenav, title) {
	// Make the title service available to the scope
	$scope.title = title;

	// Make the sidenav service available to the scope
	$scope.sidenav = sidenav;

	// Indicates that the view has not loaded yet.
	// Is initially set to false, and then changed to true once the first state is loaded.
	// Is used to display a full screen loading indicator
	$scope.appLoaded = false;

	// Is only true inbetween $stateChangeStart and $stateChangeSuccess/$stateChangeError events
	$scope.stateChanging = false;

	$scope.userIsLoggedIn = true;
	// User.isLoggedIn()
	// 	.then(function(loggedIn) {
	// 		$scope.userIsLoggedIn = loggedIn;
	// 	});
	//
	// if (!$scope.userIsLoggedIn) {
	// 	$rootScope.$on('userLoggedIn', function() {
	// 		$scope.userIsLoggedIn = true;
	// 	});
	// }


	// Listen to state events
	$rootScope.$on('$stateChangeStart', function() {
		$scope.stateChanging = true;
		$scope.stateError = false;
	});

	$rootScope.$on('$stateChangeSuccess', function() {
		$scope.appLoaded = true;
		$scope.stateChanging = false;
		$scope.errorMessage = '';
	});

	$rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
		$scope.appLoaded = true;
		$scope.stateChanging = false;
		$scope.stateError = true;
		console.error('State could not be changed due to an error');
		console.error(arguments[arguments.length - 1]);

		if (error.name) {
			if (error.name === 'MissingPermission') {
				$scope.errorMessage = 'Unfortunately you don\'t have the required permissions to access that part of the website.';
			}
			else if (error.name === 'RequiredLogin') {
				$scope.errorMessage = 'You must be logged in to access that part of the website.';
			}
		}
	});

	$rootScope.$on('$stateChangeNotFound', function(event, unfoundState) {
		$scope.appLoaded = true;
		$scope.stateChanging = false;
		console.error('Could not find state:', unfoundState.to);
	});

	// Returns true when the sidenav should be locked open
	$scope.shouldSidenavBeLocked = function() {
		return $mdMedia('gt-md');
	};

	// Returns true when the open sidenav button should be visible
	$scope.shouldSidenavToggleBeVisible = function() {
		return !$scope.shouldSidenavBeLocked();
	};

	// Toggles the left side menu.
	// Does nothing if the screen is too large as the draw is locked out anyway.
	$scope.toggleSidenav = function() {
		$mdSidenav('left').toggle();
	};

	// Redirects the app to a url
	// This can be used with ng-click instead of a href link
	$scope.goTo = function(address) {
		$location.path(address);
	};
});
