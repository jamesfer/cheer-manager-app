angular.module('cheerManager.accounts.views.summary', [
  'cheerManager.competitions'
])

.config(function($stateProvider) {
  $stateProvider
    .state('accountsSummary', {
      url: '/accounts',
      title: 'Account Summary',
      templateUrl: 'accounts-summary-view.tpl.html',
      controller: 'accountsSummaryCtrl',
      resolve: {
        competitions: function($q, Competition) {
          "ngInject";
          // Request all competition objects
          return Competition.query().$promise
            .then(function(comps) {
              var ops = [];

              // Request all members of each competition
              comps.forEach(function(comp) {
                ops.push(
                  Competition.getMembers({id: comp.id}).$promise
                    .then(function(members) {
                      // Add the members to the comp object
                      comp.members = members;
                    })
                );
              });

              // Return the comps object once all operations have resolved
              return $q.all(ops)
                .then(function() {
                  return comps;
                });
            });
        }
      }
    });
})

.controller('accountsSummaryCtrl', function($scope, competitions) {
  $scope.totalCost = 0;

  // Process the array of competitions
  competitions.forEach(function(comp) {
    var crossoverCount = 0;
    var total = 0;

    // Loop through each member
    comp.members.forEach(function(member) {
      // Add the base cost
      total += comp.entryFee;

      // And the crossover fee
      if (member.teams.length > 1) {
        ++crossoverCount;
        // Add the cost for crossovers
        total += (member.teams.length - 1) * comp.crossoverFee;
      }
    });

    comp.totalCost = total;
    comp.crossoverCount = crossoverCount;
    $scope.totalCost += total;
  });

  $scope.competitions = competitions;
});
