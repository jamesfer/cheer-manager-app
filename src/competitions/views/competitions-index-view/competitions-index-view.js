angular.module('cheerManager.competitions.views.index', [])

.controller('competitionsIndexCtrl', function($scope, $mdDialog, competitions) {
	// The list of competitions fetched during the resolve stage of the route
	$scope.competitions = competitions;
});
