angular.module('cheerManager.competitions.views', [
  'cheerManager.competitions.views.detail',
  'cheerManager.competitions.views.index',
  'cheerManager.competitions.views.edit'
]);
