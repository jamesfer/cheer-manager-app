angular.module('cheerManager.competitions.views.detail', [])

.controller('competitionsDetailCtrl', function($scope, title, competition) {
	// The value of the competition fetched during the resolve stage of the route
	// The members property contains an array of all members on this competition
	$scope.competition = competition;
	title.pushValue($scope.competition.name);
});
