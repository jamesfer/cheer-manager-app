angular.module('cheerManager.competitions.views.edit', [
	'autocompleteList'
])

.controller('competitionsEditCtrl', function($scope, $stateParams, $state, Competition, competition, allTeams) {
	$scope.isLoading = false;
	$scope.serverErrors = [];

	// The empty competition object
	$scope.competition = competition;
	$scope.allTeams = allTeams;
	$scope.isNew = true;

	// Check if an id has been specified in the url
	if ($stateParams.id !== undefined) {
		$scope.isNew = false;
	}


	// Called when the form should be submitted
	$scope.submit = function() {
		// Start loading
		$scope.isLoading = true;
		var competitionId = null;

		Competition.save($scope.competition).$promise
			.then(function(value) {
				competitionId = value.id;

				var teamIds = [];
				$scope.competition.teams.forEach(function(team) {
					teamIds.push(team.id);
				});

				// Now save the members
				return Competition.saveTeams({id: competitionId}, {teams: teamIds}).$promise;
			})
			.then(function() {
				// Success
				$scope.serverErrors = [];

				// Redirect to detail
				$state.go('competitionsDetail', {id: competitionId});
			})
			.catch(function(httpResponse) {
				// Failure
				console.log('request failed');
				if (httpResponse.data.errors) {
					$scope.serverErrors = httpResponse.data.errors;
				}
				$scope.isLoading = false;
			})
			.finally(function() {
				$scope.isLoading = false;
			});
	};

	// Called when the competition should be deleted
	$scope.delete = function() {
		// This can only be called if the competition already exists
		if (!$scope.isNew) {
			// Start loading
			$scope.isLoading = true;

			Competition.delete({id: $scope.competition.id}).$promise
				.then(function(value) {
					// Success
					$scope.serverErrors = [];

					// Redirect to index
					$state.go('competitionsIndex');
				})
				.catch(function(httpResponse) {
					console.log('Deletion failed');
				})
				.finally(function() {
					$scope.isLoading = false;
				});
		}
	};
});
