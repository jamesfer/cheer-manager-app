angular.module('cheerManager.competitions.common.states', [
  'ui.router',
  'cheerManager.competitions.common.resource'
])

.config(function($stateProvider) {
  $stateProvider
    .state('competitionsIndex', {
      url: '/competitions',
      title: 'Competition Index',
      templateUrl: 'competitions-index-view.tpl.html',
      controller: 'competitionsIndexCtrl',
      resolve: {
        competitions: function(Competition) {
          "ngInject";
          return Competition.query().$promise;
        }
      }
    })
    .state('competitionsAdd', {
      url: '/competitions/add',
      title: 'Create New Competition',
      templateUrl: 'competitions-edit-view.tpl.html',
      controller: 'competitionsEditCtrl',
      resolve: {
        competition: function() {
          // Return a blank competition object
          return {teams: []};
        },
        allTeams: function(Team) {
          "ngInject";
          return Team.query().$promise;
        }
      }
    })
    .state('competitionsDetail', {
      url: '/competitions/:id',
      title: 'Competition Detail',
      templateUrl: 'competitions-detail-view.tpl.html',
      controller: 'competitionsDetailCtrl',
      resolve: {
        competition: function($stateParams, $q, Competition) {
          "ngInject";
          // Fetches both the competition and member details.
          // Then assigns the members array onto the members property of the competition and returns the competition.
          // The final object should look like:
          // competition: {
          //   ...
          //   members: [...]
          // }
          return $q.all({
            competition: Competition.get({id: $stateParams.id}).$promise,
            competitionTeams: Competition.getTeams({id: $stateParams.id}).$promise
          }).then(function(hash) {
            hash.competition.teams = hash.competitionTeams;
            return hash.competition;
          });
        }
      }
    })
    .state('competitionsEdit', {
      url: '/competitions/:id/edit',
      title: 'Edit Competition',
      templateUrl: 'competitions-edit-view.tpl.html',
      controller: 'competitionsEditCtrl',
      resolve: {
        competition: function($stateParams, $q, Competition) {
          "ngInject";
          return $q.all({
            competition: Competition.get({id: $stateParams.id}).$promise,
            competitionTeams: Competition.getTeams({id: $stateParams.id}).$promise,
          }).then(function(hash) {
            hash.competition.teams = hash.competitionTeams;
            return hash.competition;
          });
        },
        allTeams: function(Team) {
          "ngInject";
          return Team.query().$promise
        }
      }
    });
});
