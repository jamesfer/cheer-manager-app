angular.module('cheerManager.competitions.common.resource', [
  'cheerManager.common.resourcePlus'
])

.factory('Competition', function(resourcePlus) {
  return resourcePlus('/api/competitions/:id', {id: '@id'}, {
    saveTeams: {
      method: 'PUT',
      url: '/api/competitions/:id/teams'
    },
    getTeams: {
      method: 'GET',
      url: '/api/competitions/:id/teams',
      isArray: true
    },
    getMembers: {
      method: 'GET',
      url: '/api/competitions/:id/members',
      isArray: true
    }
  });
});
